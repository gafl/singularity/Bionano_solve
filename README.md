# Container for bionano solve 3.5.1
### <br>
Bionano Solve  is an analysis pipeline for Bionano data processing, optimized for Bionano Compute and IrysSolve Compute Servers, whereas Bionano Tools contains various tools and scripts, including the Bionano Solve analysis pipeline. These tools together perform computation jobs on Saphyr and IrysSolve Compute Servers.
<br>
solve Version: 3.5.1<br>
https://bionanogenomics.com/support-page/data-analysis-documentation/<br>
https://bionanogenomics.com/wp-content/uploads/2018/04/30205-Guidelines-for-Running-Bionano-Solve-Pipeline-on-Command-Line.pdf

Singularity container based on the recipe: Singularity.solve_v3.5.1

Package installation using Miniconda2-4.7.12<br>

solve is installed in:<br>
/opt/solve<br>
subfolders:<br>
/cohortQC Scripts for generating MQR and other cohort-based metrics<br>
/FSHD Scripts for analyzing regions relevant to FSHD<br>
/HybridScaffold Scripts for single-enzyme and two-enzyme hybrid scaffold<br>
/Pipeline Scripts for de novo assembly pipeline and other utilities<br>
/RefAligner Binary tools for alignment and assembly<br>
/RefGenome CMAP files for human reference builds hg19 and hg38<br>
/SMSV Scripts for Rare Variant Pipeline<br>
/SVMerge Script for merging single-enzyme SV calls<br>
/VariantAnnotation Tools for annotating and validating SV calls<br>
/VCFConverter Scripts for converting SMAP or SVMerge files to VCF format<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
#### FIRST download the Solve3.5.1_01142020.tar.gz from Bionano site:<br>
https://bionanogenomics.com/support/software-downloads/<br>
`sudo singularity build solve_v3.5.1.sif Singularity.solve_v3.5.1`

#### ps: takes some time to build all R packages

### Get image help
`singularity run-help ./solve_v3.5.1.sif`

#### Default runscript: perl
#### Usage:
Example for hybrid scaffolding<br>
Requieres a fasta assembly file and a cmap file

`assembly_fasta_file=my_ass.fasta  #not gzipped`<br>
`cmap_file=my.cmap`<br>

`singularity exec solve_v3.5.1.sif perl /opt/solve/HybridScaffold/1.0/hybridScaffold.pl \`<br>
`-n $assembly_fasta_file \`<br>
`-b $cmap_file \`<br>
`-c /opt/solve/HybridScaffold/1.0/hybridScaffold_config.xml \`<br>
`-r /opt/solve/RefAligner/1.0/RefAligner \`<br>
`-o myoutput -f -B 2 -N 2 -p /opt/solve/Pipeline/1.0`<br>

### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull solve_v3.5.1.sif oras://registry.forgemia.inra.fr/gafl/singularity/Bionano_Solve/Bionano_Solve:latest`

